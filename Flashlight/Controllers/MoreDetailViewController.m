//
//  MoreDetailViewController.m
//  Flashlight
//
//  Created by shihaijie on 4/15/13.
//  Copyright (c) 2013 Saick. All rights reserved.
//

#import "MoreDetailViewController.h"
#import "TorchConfig.h"
#import "global_Header.h"

@interface MoreDetailViewController ()

@end

@implementation MoreDetailViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    self.clearsSelectionOnViewWillAppear = YES;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [self setTitle:@"Stop Using Torch"];
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlack];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [MobClick beginLogPageView:@"MoreDetailViewController"];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [MobClick endLogPageView:@"MoreDetailViewController"];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell_More_Detail";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    cell.accessoryType = UITableViewCellAccessoryNone;
    
    switch (indexPath.row) {
        case 0:
            cell.textLabel.text = @"5%";
            if ([TorchConfig sharedConfig].fStopTorch == 0.05f)
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            break;
            
        case 1:
            cell.textLabel.text = @"10%";
            if ([TorchConfig sharedConfig].fStopTorch == 0.1f)
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            break;
            
        case 2:
            cell.textLabel.text = @"20%";
            if ([TorchConfig sharedConfig].fStopTorch == 0.2f)
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            break;
            
        default:
            break;
    }
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
    
    switch (indexPath.row) {
        case 0:
            [TorchConfig sharedConfig].fStopTorch = 0.05f;
            break;
            
        case 1:
            [TorchConfig sharedConfig].fStopTorch = 0.1f;
            break;
            
        case 2:
            [TorchConfig sharedConfig].fStopTorch = 0.2f;
            break;
            
        default:
            break;
    }
    
    [MobClick event:@"StopUsingTorch" label:[NSString stringWithFormat:@"%.2f", [TorchConfig sharedConfig].fStopTorch]];
    
    NSArray *indexs = [NSArray arrayWithObjects:
                       [NSIndexPath indexPathForRow:0 inSection:0],
                       [NSIndexPath indexPathForRow:1 inSection:0],
                       [NSIndexPath indexPathForRow:2 inSection:0],
                       nil];
    
    NSMutableArray *oldIndex = [NSMutableArray arrayWithCapacity:2];
    for (NSIndexPath *index in indexs) {
        if (index.row != indexPath.row)
            [oldIndex addObject:index];
    }
    
    [tableView reloadRowsAtIndexPaths:oldIndex withRowAnimation:UITableViewRowAnimationNone];

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
}

@end
