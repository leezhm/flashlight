//
//  FlipsideViewController.h
//  SmartFlashlight
//
//  Created by Eric on 11/25/12.
//  Copyright (c) 2012 Saick. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@class MoreViewController;

@protocol MoreViewControllerDelegate
- (void)moreViewControllerDidFinish:(MoreViewController *)controller;
@end

@interface MoreViewController : UIViewController
<
UITableViewDataSource,
UITableViewDelegate,
MFMailComposeViewControllerDelegate
>

@property (nonatomic, retain) IBOutlet UITableView *table;

@property (assign, nonatomic) id <MoreViewControllerDelegate> delegate;

- (IBAction)done:(id)sender;

@end
