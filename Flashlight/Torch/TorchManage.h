//
//  TorchManage.h
//  Flashlight
//
//  Created by Eric on 4/14/13.
//  Copyright (c) 2013 Saick. All rights reserved.
//

#import <Foundation/Foundation.h>

@class WhiteTorchViewController;
@class SQSoundManager;

@interface TorchManage : NSObject
{
    UIWindow *_wWhite;
    WhiteTorchViewController *_vcWhiteTorch;
}

@property (nonatomic, assign) CGFloat fTorchLevel;
@property (nonatomic, retain) SQSoundManager *sound;

+ (TorchManage *)defaultManager;

- (void)showTorch:(float)fLevel;

@end
