//
//  global_Header.h
//  Flashlight
//
//  Created by Eric on 4/14/13.
//  Copyright (c) 2013 Saick. All rights reserved.
//

#ifndef Flashlight_global_Header_h
#define Flashlight_global_Header_h

#import "MobClick.h"

#define kMobClickAppKey     @"516cf1d956240bcdf10014ad"

#define kDeveloperMail      @"shjborage@gmail.com"
#define kDeveloperBlog      @"http://blog.sina.com.cn/shjborage"
#define kUserReview         @"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=635718524"

#define kNotifRestoreMainScrollPosition @"kNotifRestoreMainScrollPosition"
#define kNotifMoveOnMainScrollPosition  @"kNotifMoveOnMainScrollPosition"
#define kNotifUpdateLCD                 @"kNotifUpdateLCD"

#define kFirstInstall                   @"flash_first_install"

#endif
